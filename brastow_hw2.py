import numpy as np
import pdb
print "--"*10
print "this is the single percistion version for the (1/3)^5 with the (13/3)x[i-1] - (4/3)x[i-2] approximation"
i = 0
xnumbers = [1,1./3]
for i in range(2,6):
    x = np.float32 (((13./3)*xnumbers[i-1])-((4./3)*xnumbers[i-2]))
    xnumbers.append(x)
    #pdb.set_trace()
print "the last number is the equivolent to (1/3)^5"
print xnumbers
print "this is the approximation using (1/3)^5 as the approximation"
i = 0
x1numbers = [1.,1./3.]
for i in range(2,6):
    x1 = np.float32((1./3.)*x1numbers[i-1])
    x1numbers.append(x1)
print "the last number is the equivolent to (1/3)^5"
print x1numbers
print "-"*10
print "--"*10
print "this is the double percistion version for the (1/3)^5"
i = 0
ynumbers = [1,1./3]
for i in range(2,6):
    y = np.float64(((13./3)*ynumbers[i-1])-((4./3)*ynumbers[i-2]))
    ynumbers.append(y)
    #pdb.set_trace()
print "the last number is the equivolent to (1/3)^5"
print ynumbers
print "this is the approximation using (1/3)^5 as the approximation"
i = 0
y1numbers = [1.,1./3.]
for i in range(2,6):
    y1 = np.float64((1./3.)*y1numbers[i-1])
    y1numbers.append(y1)
print "the last number is the equivolent to (1/3)^5"
print y1numbers
print "-"*10
print "--"*10
xy = xnumbers[-1] - ynumbers [-1]
xyy = xy/ynumbers[-1]
x1y1 = x1numbers[-1] - y1numbers[-1]
x1y1y1 = x1y1 / y1numbers[-1]
print "if you look at the last values of both of the lists then you can notice a discrepency that gives the absolute error of %s and a relitive error of %s.  Then for the second version of approximation the absolute error is %s and the relitive error is %s." % (xy,xyy,x1y1,x1y1y1)
print "-"*10
print "--"*10
print "this is the single percistion version for the (1/3)^20 using the (13/3)x -(4/3)x approximation"
i = 0
znumbers = [1,1./3]
for i in range(2,21):
    z = np.float32 (((13./3)*znumbers[i-1])-((4./3)*znumbers[i-2]))
    znumbers.append(z)
print "this number is the single percision equivolent to (1/3)^20"
print znumbers[-1]
i = 0
z1numbers = [1,1./3]
for i in range (2,21):
    z1 = np.float32((1./3)*z1numbers[i-1])
    z1numbers.append(z1)
print "this number is the equivolent to (1/3)^20"
print z1numbers[-1]
print "-"*10
print "--"*10
print "this is the double percistion version for the (1/3)^20"
i = 0
nnumbers = [1.,1./3]
for i in range(2,21):
    n = np.float64(((13./3)*nnumbers[i-1])-((4./3)*nnumbers[i-2]))
    nnumbers.append(n)
    #pdb.set_trace()
print "this is the double percision equivolent to (1/3)^20"
print nnumbers[-1]
print "***** Note ***** after testing the code, the error starts blowing up on the 15th iteration where it inexplicably starts to diverge from the expected value.  In the 16th iteration the values inexplicably turn negitive rather than remain positive.  This is similar to when you do the singe percision but for the double percision this is to a lesser extent and it manifests itself later in the itterations than in the single percision code."
i = 0
n1numbers = [1,1./3]
for i in range(2,21):
    n1 = np.float64((1./3)*n1numbers[i-1])
    n1numbers.append(n1)
print "this number is the equivolent version of the double percsion number taken to the (1/3)^20 exactly"
print n1numbers[-1]
print "-"*10
print "--"*10
zn = znumbers[-1] - nnumbers [-1]
znn = zn/nnumbers[-1]
z1n1 = z1numbers[-1] - n1numbers[-1]
z1n1n1 = z1n1 / n1numbers[-1]
print "if you take these numbers and subtact them then you get the absolute error of %s and the relitive error of %s for the (13/3)x - (4/3)x approximation and you get the absolute error of %s and the relitive error of %s for the direct calculation of (1/3)^20" % (zn,znn,z1n1,z1n1n1)
print "-"*10
print "--"*10
x2numbers = [1.,4.]
for i in range(2,21):
    x2 = np.float32(4.*x2numbers[i-1])
    x2numbers.append(x2)
print "this is the single percision equivolent of 4^n"
print x2numbers[-1]
x3numbers = [1.,4.]
for i in range(2,21):
    x3 = np.float64(4.*x3numbers[i-1])
    x3numbers.append(x3)
print "this is the double percision equivolent of 4^n"
print x3numbers[-1]
x2x3 = x2numbers[-1] - x3numbers[-1]
x2x3x3 = x2x3 / x3numbers[-1]
print "This calculation is stable because there are no fractions involved in it.  Since there are only intiger numbers then the error would not cause a divergance like what would happen when you have a fraction.  Absolute and reletive error still are somewhat relivent but not as much since there is little divergence between the actual value and the calculated value.  You would use the relitive error would be the most applicable."
