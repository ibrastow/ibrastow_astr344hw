import numpy as np
#Got help from John Dominguez with debugging
x1 = 0.0
x2 = 0.0
x = 2.0

def f(x):
    # Roots from 2,4 is 3.14 .... , pi
    return np.sin(x)
def g(x):
    # Root from 1,2 is 1.52138...
    return (x**3)-x-2
def y(x):
    # Roots from 0,5 is 2
    return -6 + x + (x**2)
def root(func):
    global x1
    global x2
    global x
    if func(x1) > 0:
        if func(x1 + (x2 - x1)/2.0) > 0:
            x1 = x1 + (x2 - x1)/2.0
            x = x1
        else:
            x2 = x2 - (x2 -x1)/2.0
            x = x2
    else:
        if func(x1 + (x2-x1)/2.0) < 0:
            x1 = x1 + (x2-x1)/2.0
            x = x1
        else:
            x2 = x2 - (x2 - x1)/2.0
            x = x2
        
x1 = 2.0
print "this is x1"
print x1
x2 = 4.0
print "this is x2"
print x2
tolerance = (10.0**-4)
print "this is the tolerance"
print tolerance
while abs(f(x)) >= tolerance:
    root(f)
    

print "this is the value of x, the root, when you choose the bounds of 2 and 4 for the function sin(x)"
print x
print "--"*10

x1 = 1.0
print "this is x1"
print x1
x2 = 2.0
print "this is x2"
print x2
tolerance = (10**-4)
print "this is tolerance"
print tolerance
while abs(g(x)) >= tolerance:
    root(g)

print "this is the value x, the root, when you choose the bounds of 1 and 2 for the function (x**3)-x-2"
print x
print "--"*10

x1 = 0.0
print "this is x1"
print x1
x2 = 5.0
print "this is x2"
print x2
tolerance = (10**-4)
print "this is the tolerance"
print tolerance
while abs(y(x)) >= tolerance:
    root(y)

print "this is the value x, the root, when you choose the bounds of 0 and 5 for the function -6 + x + (x**2)"
print x
print "--"*10
