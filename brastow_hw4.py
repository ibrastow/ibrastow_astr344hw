import numpy as np
import matplotlib.pyplot as plt
def integral(start,end,nspaces):
    xi = np.linspace(start, end, num = nspaces)
    integral_value = 0
    fxi = []
    c = 3000.
    OmegaM = 0.3
    OmegaA = 0.7
    for i in range(0,nspaces):
        fxi_value = (c / (OmegaA + OmegaM * (1 + xi[i])**3)**0.5)
        fxi.append(fxi_value)
    for i in range(0,nspaces -1):
        a = 0.5 * (xi[i+1]-xi[i]) * (fxi[i] +fxi[i+1])
        integral_value = integral_value + a
    return integral_value
def D_sub_a(z):
    return integral(0,z,3000)/(1+z)
D_values = []
xi = np.linspace(0, 5, 3000-1)
for j in range(0,3000-1):
    D = D_sub_a(xi[j])
    D_values.append(D)

print "I start of by making the definition of a function called 'integral' with the inputs of the starting number ending number and the number of spaces."
print "In my definition of the function 'integral' I started by creating a list of evenspaced values of z called xi using np.linspace."
print "I then set up the starting value of the integral to be 0 and created an empty list called fxi which will store my values of H(z)."
print "I then defined the values for the constants which I will need later in the definition."
print "I then used a for loop to apply the function H(z) to all of the values of z which are in xi."
print "I then used a for loop to use the trapazoidal integral of the function H(z)"
print "I then have the function return the value of 'integral_value' which in the previous for loop kept track of the total summation under the curve."
print "I then made the definition called 'D_sub_a' which will return the integral from 0 to z for 3000 divisions all devided by 1 + z."
print "I then made the list called 'D_values' which will be used to keep track of all the values made by applying D_sub_a to all the values of xi."
print "this allows me to plot the z values as the x axis and the D_sub_a values on the y axis"
plt.plot(xi,D_values)
plt.title('plot of my integral')
plt.ylabel('D_sub_a')

plt.show()
