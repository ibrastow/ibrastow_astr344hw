import numpy as np
from datetime import datetime
from datetime import timedelta
# used http://stackoverflow.com/questions/12131766/python-date-time-comparison-using-timestamps-timedelta Answer 3 for time help

x1 = 0.0
x2 = 0.0
x = 2.0

def f(x):
    # Roots from 2,4 is 3.14 .... , pi
    return np.sin(x)
def g(x):
    # Root from 1,2 is 1.52138...
    return (x**3)-x-2
def y(x):
    # Roots from 0,5 is 2
    return -6 + x + (x**2)
def root(func):
    global x1
    global x2
    global x
    if func(x1) > 0:
        if func(x1 + (x2 - x1)/2.0) > 0:
            x1 = x1 + (x2 - x1)/2.0
            x = x1
        else:
            x2 = x2 - (x2 -x1)/2.0
            x = x2
    else:
        if func(x1 + (x2-x1)/2.0) < 0:
            x1 = x1 + (x2-x1)/2.0
            x = x1
        else:
            x2 = x2 - (x2 - x1)/2.0
            x = x2
        
x1 = 2.0
print "this is x1"
print x1
x2 = 4.0
print "this is x2"
print x2
tolerance = (10.0**-4)
print "this is the tolerance"
print tolerance
now1 = datetime.now()
while abs(f(x)) >= tolerance:
    root(f)
    
then1 = datetime.now()
tdelta1 = then1 - now1
seconds1 = tdelta1.total_seconds()
print "this is the value of x, the root, when you choose the bounds of 2 and 4 for the function sin(x)"
print x
print "this code ran in"
print "%s seconds"%(seconds1)
print "--"*10

x1 = 1.0
print "this is x1"
print x1
x2 = 2.0
print "this is x2"
print x2
tolerance = (10**-4)
print "this is tolerance"
print tolerance
now2 = datetime.now()
while abs(g(x)) >= tolerance:
    root(g)
then2 = datetime.now()
tdelta2 = then2 - now2
seconds2 = tdelta2.total_seconds()
print "this is the value x, the root, when you choose the bounds of 1 and 2 for the function (x**3)-x-2"
print x
print "this code ran in"
print "%s seconds"%(seconds2)
print "--"*10

x1 = 0.0
print "this is x1"
print x1
x2 = 5.0
print "this is x2"
print x2
tolerance = (10**-4)
print "this is the tolerance"
print tolerance
now3 = datetime.now()
while abs(y(x)) >= tolerance:
    root(y)
then3 = datetime.now()
tdelta3 = then3 - now3
seconds3 = tdelta3.total_seconds()
print "this is the value x, the root, when you choose the bounds of 0 and 5 for the function -6 + x + (x**2)"
print x
print "this code ran in"
print "%s seconds"%(seconds3) 
print "--"*10
########################################################
def deriv(func,xi,dx):
	y = (func(xi + dx) - func(xi))/(dx)
	return y
########################################################
xi = 0
def NR(func):
	global xi
	xi = xi - (func(xi)/deriv(func,xi,0.000001))
	return xi
########################################################
tolerance = 10**-4
xi = 2
print "%s is the starting initial guess and %s is the tolerance"%(xi,tolerance)
now4 = datetime.now()
while abs(f(xi)) >= tolerance:
	NR(f)
then4 = datetime.now()
tdelta4 = then4 - now4
seconds4 = tdelta4.total_seconds()
print "this is the value of x, the root, when you choose the initial starting value of 2 for the function sin(x)"
print xi
print "this code ran in %s seconds"%(seconds4)
print "--"*10

tolerance = 10**-4
xi = 1
print "%s is the starting initial guess and %s is the tolerance"%(xi,tolerance)
now5 = datetime.now()
while abs(g(xi)) >= tolerance:
	NR(g)
then5 = datetime.now()
tdelta5 = then5 - now5
seconds5 = tdelta5.total_seconds()
print "this is the value of x, the root, when you choose the initial starting value of 1 for the function (x**3)-x-2"
print xi
print "this code ran in %s seconds"%(seconds5)
print "--"*10

tolerance = 10**-4
xi = 0
print "%s is the starting initial guess as %s is the tolerance"%(xi,tolerance)
now6 = datetime.now()
while abs(y(xi)) >= tolerance:
	NR(y)
then6 = datetime.now()
tdelta6 = then6- now6
seconds6 = tdelta6.total_seconds()
print "this is the value of x, the root, when you choose the initial starting value of 0 for the function -6+x+(x**2)"
print xi
print "this code ran in %s seconds"%(seconds6)
print "--" *10

########################################################
import math as ma
h = 6.6260755 * (10**-27)
c = 2.99792458 * (10**10)
nu = 870 * (10**-6)
Bt = 1.25 * (10**-12)
k = 1.380658 * (10**-16)
xi = 30
tolerance = 10**-20
def plank(xi):
	return ((((2. * h * (nu**3.))/(c**2.))/(ma.exp((h * nu)/(xi * k)) - 1.)) - Bt)
while abs(plank(xi)) >= tolerance:
	NR(plank)
print xi	
print "((((2. * h * (nu**3.))/(c**2.))/(ma.exp((h * nu)/(xi * k)) - 1.)) - Bt)"
print ((((2. * h * (nu**3.))/(c**2.))/(ma.exp((h * nu)/(xi * k)) - 1.)) - Bt)
print "h"
print h
pritn "c"
print c
print "nu"
print nu
print "intensity"
print Bt
print "Boltzman's constant"
print k
print "planck function : print (((2. * h * (nu**3.))/(c**2.))/(ma.exp((h * nu)/(xi * k)) - 1.))"
print (((2. * h * (nu**3.))/(c**2.))/(ma.exp((h * nu)/(xi * k)) - 1.))