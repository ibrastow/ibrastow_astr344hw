import numpy as np
import math as ma
import time

#the number of people valid for becomeing infected(i.e. the amount of people who arent vaccinated)
number_people = 100

xpositions = []
ypositions = []

# this sets everyones infected status equal to false except for the first person
infected = [1]
for j in range(1,number_people):
        sick = 0
        infected.append(sick)


timesteps = 0

#this creates a list of every person's x and y coordinates in a random distribution from 0 to 100.  Each element is a different person's
# position.
for i in range(0,number_people):
	xpos = 100 * (np.random.random())
	ypos = 100 * (np.random.random())
	xpositions.append(xpos)
	ypositions.append(ypos)

#this is a the move function in which each person's x and y coordinates move in a random direction from -3 to 3 in all directions.
#if any person attempts to escape the room by moving outside [0,100] in either direction, then it sets their respective position to
#the closest valid x and/or y position
while sum(infected) < number_people:
        timesteps = timesteps +1
        print timesteps
	for i in range(0,number_people):
		xpos = xpositions[i] + ( 5 * (np.random.random())) - (5 * (np.random.random()))
		ypos = ypositions[i] + ( 5 * (np.random.random())) - (5 * (np.random.random()))
		if xpos < 0:
			xpos = 0
			xpositions.append(xpos)
			if ypos < 0:
                                ypos = 0
				ypositions.append(ypos)
			if ypos > 100:
                                ypos = 100
				ypositions.append(ypos)
			else:
				ypositions[i] = ypos
		if xpos > 100:
                        xpos = 100
			xpositions.append(100)
			if ypos < 0:
                                ypos = 0
				ypositions.append(ypos)
			if ypos > 100:
                                ypos = 100
				ypositions.append(ypos)
			else:
				ypositions[i] = ypos
		if ypos < 0:
                        ypos = 0
			ypositions.append(ypos)
			xpositions[i] = xpos
		if ypos > 100:
                        ypos = 100
			ypositions.append(ypos)
			xpositions[i] = xpos
		else:
			xpositions[i] = xpos
			ypositions[i] = ypos
                print xpositions[i]
                print ypositions[i]
                if infected[i] == 1:
                        for z in range(0,number_people):
                                if (xpositions[i] - xpositions[z])**2 + (ypositions[i] - ypositions[z])**2 <= 3:
                                        if infected[z] == 0:
                                                if 100 * np.random.random() < 75:
                                                        infected[z] = 1
                if infected[i] == 0:
                        for z in range(0,number_people):
                                if (xpositions[i] - xpositions[z])**2 + (ypositions[i] - ypositions[z])**2 <= 3:
                                        if infected[z] == 1:
                                                if 100 * np.random.random() < 75:
                                                        infected[i] = 1
        print infected
        print sum(infected)
print timesteps

